cmake_minimum_required(VERSION 2.8.3)
project(coe_wago_plugin)


option( WITH_SOEM "Compile with SOEM" ON)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)
set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_VERBOSE_MAKEFILE OFF)


find_package(catkin REQUIRED COMPONENTS
  coe_hw_plugins coe_driver coe_core realtime_tools message_generation std_msgs std_srvs pluginlib
)

## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS system)


catkin_package(
  LIBRARIES coe_wago_plugin
  CATKIN_DEPENDS coe_hw_plugins coe_core message_runtime pluginlib
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
add_library(${PROJECT_NAME}
   src/${PROJECT_NAME}/coe_wago_plugin.cpp 
)

if( WITH_SOEM )
    MESSAGE("SOEM is USED")
    add_definitions(-D__USE_SOEM__ )
    set( rt_LIBRARIES soem
                      pthread
                      rt )
endif()

add_executable(${PROJECT_NAME}_node src/coe_wago_plugin_node.cpp)
add_dependencies(${PROJECT_NAME}_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
target_link_libraries(${PROJECT_NAME}_node ${PROJECT_NAME} ${catkin_LIBRARIES} ${rt_LIBRARIES} )
