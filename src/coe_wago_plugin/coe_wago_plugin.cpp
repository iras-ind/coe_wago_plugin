
#include <pluginlib/class_list_macros.h>
#include <std_msgs/Float64MultiArray.h>
#include <realtime_tools/realtime_publisher.h>

#include <coe_core/coe_base.h>
#include <coe_core/ds301/coe_bitwise_struct.h>
#include <coe_core/ds301/coe_sdo_dictionary.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>
#include <coe_core/ds402/coe_xfsm_symbols.h>
#include <coe_core/ds402/coe_bitwise_struct.h>


#include <coe_driver/coe_hw_base_plugin.h>

namespace coe_wago_plugin
{

class WagoAdditube : public coe_driver::CoeHwPlugin
{
private:

  std::map< std::string, double >                        analog_inputs_;
  std::map< std::string, double >                        analog_outputs_;
  std::map< std::string, bool   >                        digital_inputs_;
  std::map< std::string, bool   >                        digital_outputs_;
  
  virtual void readDiagnostics (diagnostic_updater::DiagnosticStatusWrapper &stat) {};
  virtual void writeDiagnostics(diagnostic_updater::DiagnosticStatusWrapper &stat) {};
  
public:

  WagoAdditube( ) { }
  
  bool initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address);
  bool read();
  bool write();

  bool                         setState              ( const std::string& state_name );
  std::vector<std::string>     getStateNames         ( ) const;
  std::string                  getActualState        ( );
  std::vector<std::string>     getErrors             ( ) ;
  
  std::vector<std::string>     getAnalogInputNames   ( ) const;
  std::vector<std::string>     getAnalogOutputNames  ( ) const;
  std::vector<std::string>     getDigitalInputNames  ( ) const;
  std::vector<std::string>     getDigitalOutputNames ( ) const;

  void  getAnalogInputValuePtr    ( const std::string& name, double*& input  );
  void  getAnalogOutputValuePtr   ( const std::string& name, double*& output );
  
  void  getDigitalInputValuePtr   ( const std::string& name, bool*& input  );
  void  getDigitalOutputValuePtr  ( const std::string& name, bool*& output );

  bool                             setControlWord       ( const coe_core::ds402::control_word_t& control_word ) { throw std::runtime_error("Not supported for this implementation."); };
  coe_core::ds402::status_word_t   getStatusWord        ( )                                               const { throw std::runtime_error("Not supported for this implementation."); }
  coe_core::ds402::control_word_t  getControlWord       ( )                                               const { throw std::runtime_error("Not supported for this implementation."); }
  
  
  void  getStateWordPtr           (uint16*& status_word )                                                       { throw std::runtime_error("Not supported for this implementation."); }
  void  getControlWordPtr         (uint16*& control_word )                                                      { throw std::runtime_error("Not supported for this implementation."); }

  void  getJointStatePtr          (double*& pos,  double*& vel, double*& eff)                                   { throw std::runtime_error("Not supported for this implementation."); }
  void  getJointCommandPtr        (double*& pos,  double*& vel, double*& eff)                                   { throw std::runtime_error("Not supported for this implementation."); }
  
  virtual bool hasDigitalInputs(){return true;};
  virtual bool hasDigitalOutputs(){return true;};
  virtual bool hasAnalogInputs(){return true;};
  virtual bool hasAnalogOutputs(){return true;};
  virtual bool isActuator(){return false;};
  
  

};




///////////////////////////////////////
bool WagoAdditube::initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address)
{
  if (!CoeHwPlugin::initialize(nh, device_coe_parameter, address) )
  return false;
  
  //Pointer to 
  for( auto const & a_i : module_->getAnalogInputs  ( ) ) analog_inputs_  [ a_i.first ] = 0.0;
  
  //Pointer to 
  for( auto const & a_o : module_->getAnalogOutputs ( ) ) analog_outputs_ [ a_o.first ] = 0.0;
  
  //Pointer to 
  for( auto const & d_i : module_->getDigitalInputs  ( ) ) digital_inputs_  [ d_i.first ] = 0;
  
  //Pointer to 
  for( auto const & d_o : module_->getDigitalOutputs ( ) ) digital_outputs_ [ d_o.first ] = 0;

  // Init values
  bool ret =  read() 
           && write();    
  
  return ret;
}


std::vector<std::string>   WagoAdditube::getStateNames ( ) const 
{ 
  return std::vector<std::string>{ "Not supported" }; 
}
std::string WagoAdditube::getActualState ( )  
{ 
  return "Not supported" ; 
}

std::vector<std::string> WagoAdditube::getAnalogInputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getAnalogInputs()   ) 
    n.push_back(m.first); 
  return n; 
}

std::vector<std::string> WagoAdditube::getAnalogOutputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getAnalogOutputs()   ) 
    n.push_back(m.first); 
  return n; 
}

std::vector<std::string> WagoAdditube::getDigitalInputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getDigitalInputs()   ) 
    n.push_back(m.first); 
  return n; 
}

std::vector<std::string> WagoAdditube::getDigitalOutputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getDigitalOutputs()   ) 
    n.push_back(m.first); 
  return n; 
}

void  WagoAdditube::getAnalogInputValuePtr  (const std::string& name, double*& input ) 
{
  input = &( analog_inputs_[name] );
}

void  WagoAdditube::getAnalogOutputValuePtr  (const std::string& name, double*& output ) 
{
  output = &( analog_outputs_[name] );
}

void  WagoAdditube::getDigitalInputValuePtr  (const std::string& name, bool*& input ) 
{
  input = &( digital_inputs_[name] );
}

void  WagoAdditube::getDigitalOutputValuePtr  (const std::string& name, bool*& output ) 
{
  output = &( digital_outputs_[name] );
}

std::vector<std::string>   WagoAdditube::getErrors ( )  
{ 
  return std::vector<std::string>{ "Not yet supported" }; 
}

bool WagoAdditube::setState( const std::string& state_name )
{
  return true;
}



bool WagoAdditube::read()
{
  
  if( !CoeHwPlugin::read() )
    return false;
  
  for( auto & a_i  :  module_->getAnalogInputs(  ) )
  { 
    analog_inputs_[ a_i.first ] = a_i.second.entry->get<uint16_t>( ) * a_i.second.scale +  a_i.second.offset;
  }  
  
  for( auto & d_i  :  module_->getDigitalInputs(  ) )
  { 
    digital_inputs_[ d_i.first ] = d_i.second.entry->get<bool>( ) ;
  }
  return true;
}
bool WagoAdditube::write()
{
  

  for( auto & d_o  :  module_->getDigitalOutputs(  ) )
  { 
    d_o.second.entry->set<bool>( digital_outputs_[ d_o.first ] );
  }
  
  for( auto & a_o  :  module_->getAnalogOutputs(  ) )
  { 
    a_o.second.entry->set<uint16_t>( analog_outputs_[ a_o.first ] );
  }

  return CoeHwPlugin::write();
  
}



  ///////////////////////////////////////


  PLUGINLIB_EXPORT_CLASS(coe_wago_plugin::WagoAdditube, coe_driver::CoeHwPlugin );


}
