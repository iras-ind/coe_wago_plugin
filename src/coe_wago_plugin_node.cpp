#include <signal.h>

#include <ros/ros.h>
#include <pluginlib/class_loader.h>
#include <realtime_tools/realtime_publisher.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_srvs/Trigger.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int8MultiArray.h>

#include <itia_futils/itia_futils.h>
#include <coe_driver/CoeMessage.h>
#include <coe_driver/Ds402Command.h>
#include <coe_driver/Ds402States.h>
#include <coe_driver/CoeErrors.h>
#include <coe_driver/coe_hw_base_plugin.h>

class TestPlugin
{
private:
  
  std::vector<double*>  ai_msr;
  std::vector<double*>  ao_cmd;
  std::vector<bool*>    di_msr;
  std::vector<bool*>    do_cmd;
  
  pluginlib::ClassLoader<coe_driver::CoeHwPlugin> loader_;
  boost::shared_ptr<coe_driver::CoeHwPlugin>      WagoPlugin_;

  
public:
  TestPlugin( ros::NodeHandle& nh ) : loader_( "coe_hw_base_plugin", "coe_driver::CoeHwPlugin" ), ai_msr(8,nullptr),ao_cmd(8,nullptr),di_msr(8,nullptr), do_cmd(8,nullptr)
  {
    try
    {
      
        WagoPlugin_ = loader_.createInstance ( "coe_wagp_plugin::WagoAdditube" );
        WagoPlugin_->initialize ( nh, "/coe_driver_node/coe/", 7);

        //---
        for( size_t i=0; i< ai_msr.size(); i++ )
        {
          WagoPlugin_->getAnalogInputValuePtr( "AI"+ std::to_string(i+1),  ai_msr[ i ]);
        }
        
        //---
        for( size_t i=0; i< ao_cmd.size(); i++ )
        {
          WagoPlugin_->getAnalogOutputValuePtr( "AO"+ std::to_string(i+1),  ao_cmd[ i ]);
        }
        
        //---
        for( size_t i=0; i<di_msr.size(); i++ )
        {
          WagoPlugin_->getDigitalInputValuePtr( "DI"+ std::to_string(i+1),  di_msr[ i ]);
        }

        //---
        for( size_t i=0; i< do_cmd.size(); i++ )
        { 
          WagoPlugin_->getDigitalOutputValuePtr( "DO"+ std::to_string(i+1), do_cmd[ i ]);
        }
        
                  
    }
    catch ( pluginlib::PluginlibException& ex )
    {
        ROS_ERROR ( "The plugin failed to load for some reason. Error: %s", ex.what() );
    }

  }
  ~TestPlugin() 
  {

  }
  
  
  bool unload( )
  {
    try
    {
        ROS_INFO("%sReset the plugin%s",BOLDMAGENTA,RESET);
        WagoPlugin_.reset();
        ROS_INFO("%sForce the unload%s",BOLDMAGENTA,RESET);
        loader_.unloadLibraryForClass("coe_wago_plugin::WagoAdditube");
        ROS_INFO("%sDone unload%s",BOLDMAGENTA,RESET);
    }
    catch ( pluginlib::PluginlibException& ex )
    {
        ROS_ERROR ( "The plugin failed to load for some reason. Error: %s", ex.what() );
        return false;
    }
    return true;
  }
  
  bool getOperationMode ( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
  {
    res.message = WagoPlugin_->getActualState();
    res.success = true; 
    return true;
  }
  
  bool getCoeErrors ( coe_driver::CoeErrors::Request& req, coe_driver::CoeErrors::Response& res )
  {
    res.message = WagoPlugin_->getErrors();
    for( auto const & m : res.message ) std::cout << m << std::endl;
    return (res.success = true);
  }
  
  void setDigitalOutput(const std_msgs::Int8MultiArray::ConstPtr& msg)
  {
    std::cout << "Dig Out:" << *msg << "(" << std::min( msg->data.size(),do_cmd.size() ) << ")" <<std::endl;
    for( size_t i=0; i <std::min( msg->data.size(),do_cmd.size() ); i++ )
    {
      *(do_cmd[i]) = msg->data[i];
    }
  }

  void setAnalgoOutput(const std_msgs::Float64MultiArray::ConstPtr& msg)
  {
    for( size_t i=0; i <std::min( msg->data.size(),do_cmd.size() ); i++ )
    {
      *(ao_cmd[i]) = msg->data[i];
    }
  }

  bool sync()
  {
    
    WagoPlugin_->read();
    WagoPlugin_->write();  

    return true;
  }
  
  const std::vector<double*>& getAI() const { return ai_msr; }
  const std::vector<double*>& getAO() const { return ao_cmd; }
  const std::vector<bool*>&   getDI() const { return di_msr; }
  const std::vector<bool*>&   getDO() const { return do_cmd; }  
};

bool doShutdown = false;
void shutdown(int sig)
{
  ROS_INFO("%sUser Shutdown%s",BOLDMAGENTA,RESET);
  doShutdown = true;
  
}


int main ( int argc, char* argv[] )
{
    ros::init ( argc,argv, "wago_hw_plugin_test", ros::init_options::NoSigintHandler );
    ros::NodeHandle nh("~");
    
    signal(SIGINT, shutdown);

    TestPlugin* test = new TestPlugin( nh );
    
    ros::AsyncSpinner spinner(4);
    spinner.start();
    
    ros::ServiceServer coe_moo_service_     = nh.advertiseService                       ( "get_operation_mode", &TestPlugin::getOperationMode, test);
    ros::ServiceServer coe_errors_service_  = nh.advertiseService                       ( "get_errors",         &TestPlugin::getCoeErrors,     test);
    
    ros::Subscriber    coe_digout_          = nh.subscribe<std_msgs::Int8MultiArray>    ( "set_do",   1 ,       &TestPlugin::setDigitalOutput, test);
    ros::Subscriber    coe_anaout_          = nh.subscribe<std_msgs::Float64MultiArray> ( "set_ao",   1 ,       &TestPlugin::setAnalgoOutput,  test);
    
    
    realtime_tools::RealtimePublisher< std_msgs::Int8MultiArray    > di_pub(nh, "get_di", 4); 
    realtime_tools::RealtimePublisher< std_msgs::Float64MultiArray > ai_pub(nh, "get_ai", 4);
    
    ros::WallRate rt(1000);
    while( 1 )
    {
      test->sync();
         
      if( ai_pub.trylock() )
      {
        ai_pub.msg_.data.clear();
        for( auto const & a_i :  test->getAI() ) ai_pub.msg_.data.push_back  ( *a_i );
        ai_pub.unlockAndPublish();
      }

      if( di_pub.trylock() )
      {
        di_pub.msg_.data.clear();
        for( auto const & d_i : test->getDI() )  di_pub.msg_.data.push_back  ( *d_i );
        
        di_pub.unlockAndPublish();
      }
      
      if( doShutdown )
      {
        ROS_INFO("%sTerminate node%s",BOLDMAGENTA,RESET);
        break;
      }
      else
      {
        rt.sleep();
      }
    }
    
    ROS_INFO("%sKill services%s",BOLDMAGENTA,RESET);
    coe_moo_service_.shutdown();
    coe_errors_service_.shutdown();
    ROS_INFO("%sStop Publishers%s",BOLDMAGENTA,RESET);
    ai_pub.stop();
    di_pub.stop();
    
    ROS_INFO("%sUnload the plugin%s",BOLDMAGENTA,RESET);
    test->unload();

    ROS_INFO("Bye Bye");
    
    delete test;
    
    return 0;
}


